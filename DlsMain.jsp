<%@ page language="java" contentType = "text/html;charset=KSC5601" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>디지털물류시스템 (Digital Logistics System)</title>
<style type="text/css">
<!--
/* 공통 제어 시작 */
   html,body {width:100%;height:100%;}
	html,body,div,ul,li,dl,dt,dd,ol,p,h1,h2,h3,h4,h5,h6,form {margin:0;padding:0;font:normal 11px dotum,'돋움',verdana;color:#cedff5;line-height:1.5em;}
	ul,ol,dl {list-style:none}
	img {border:0;vertical-align:top;}
	a {font:normal 11px dotum,'돋움';color:#cedff5 ;text-decoration:none;}
	a:hover {font:normal 11px dotum,'돋움';color:#cedff5;text-decoration:underline;}
/* 공통 제어 마침 */

#wrap {
	position: relative;
	top:45%;
	left:50%;
	width:556px;
	height:393px;
	margin-left:-278px; /*** width 값의 1/2 ***/
	margin-top:-197px; /*** height 값의 1/2 ***/
	}

.contents{
	background:url(images/intro/Visual_dls.jpg) no-repeat;
	width:556px;
	height:393px;
	}
.sub_btn{
	padding: 17px 18px 0 0;
	float:right;	
	}	
	
.login{
	padding:270px 0 0 352px ;
	width: 185px;	
	}
.login input.input01{
	float:left;
	vertical-align:top;
	color:#2c4eae;
	font-size:12px;
	background:#deeaf8;
	border-top:#3170bd solid 1px;
	border-left:#3170bd solid 1px;
	border-bottom:#deeaf8 solid 1px;
	border-right:#deeaf8 solid 1px;
	width:108px;
	height:18px;
	margin:5px 8px 0 0;
	padding:6px 0 0 3px;
}
.login input.btn{
	margin: 5px 0;
	*margin-top:-27px;
	}
	
.login a {
	text-decoration: underline;
	letter-spacing:-1px;
	float:right;
	width:65px;
	}

/* 마우스 비클릭시 나오는 글자 이미지 배경 설정 */
#uid {background:#deeaf8 url(images/intro/id.gif) no-repeat 0px 0px;color:#898989;}
#upw {background:#deeaf8 url(images/intro/pw.gif) no-repeat 0px 0px;color:#898989;}

/* 마우스 클릭시 클릭아닐때의 모습 설정 */
.focus{background:#deeaf8 !important;}
.focusnot{background:#deeaf8 !important;}

//-->
</style>
<%
	String msgtype 		= request.getParameter("msgtype");
	String remainDay	= request.getParameter("remainDay");	
	String Url = HttpUtils.getRequestURL(request).toString();
	
	System.out.println("1 Url : "+ Url);	
	
%>

<script language="JavaScript">
<!--
	function login(){
		
		document.form.target="_parent"
		document.form.action="/servlets/management/LogInOutServlet4?command=login"
		document.form.submit();
	}
	
	function changepwd() {
		change=window.open("/hjdls/changepasswd.jsp", "change", "height=202,width=320,top=240,left=380");
	}

// -->
</script>
</head>

<body>
<div id="wrap">
	<div class="contents">		
		<div class="login">
		<form name=form method="post">			
		<p>
			<input type="text" name="id" class="input01" id="uid" onfocus="this.className='input01 focus';" onblur="if (this.value.length==0) {this.className='input01';}else {this.className='input01 focusnot';}" />
		</p>
		<p>
			<input type="password" name="password" value="" class="input01" maxlength="12" id="upw" onfocus="this.className='input01 focus';" onblur="if (this.value.length==0) {this.className='input01';}else {this.className='input01 focusnot';}" >
		</p>
		
		<input type="image" src="images/intro/btn_login.gif" class="btn" onClick="login()"/>
		</form>
		<a href="javascript:changepwd();">비밀번호 변경</a>
		
		</div>             	
	</div>
</div>
</body>
</html>
