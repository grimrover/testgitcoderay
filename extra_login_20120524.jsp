<%@ page language="java" contentType = "text/html;charset=KSC5601" %>
<%@ page import="dls.management.*" %>
<%@ page import="dls.frame.util.*" %>
<html>
<head>
<title>HANJIN Extranet Login</title>
<style type="text/css">
<!--
.input {
	font: 12px "굴림";
	color: #333333;
	border: 1px solid #FFFFFF;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) { //v4.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && document.getElementById) x=document.getElementById(n); return x;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	function move(a)
	{
	   if (a == 13){
			login();
	   }
	}

	function login() {
		document.form.target="_self";
		document.form.action="/servlets/management/LogInOutServlet3";
		document.form.submit();
	}

	function changepwd() {
		window.location.replace("/extra/extra_index.jsp?flag=pwChange");
	}
	
	function movePage(){
		window.location.replace("/extra/extra_index.jsp");
	}
	
	function setModify()
	{
		alert("○ 패스워드 정책 Rule 적용사항\n    ① 패스워드 정책 적용일: 2010년 9월 1일부\n\n    ② 패스워드 최소자리 : 6 자리 (최대 8자리)\n\n    ③ 패스워드 변경주기 : 30일 이하\n\n    ④ 패스워드 변경이력 : 직전 3개 패스워드 재사용금지\n\n    ⑤ 패스워드 입력실패 : 5회 이상 접근시도 실패 시 비활성화\n\n    ⑥ 패스워드 Rule : 문자 · 숫자조합, ID와 동일 패스워드 금지\n\n    ⑦ 개인정보항목 패스워드 사용 금지(주민등록번호 등)\n\n ○ 패스워드 비활성화 시[비밀번호 초기화](오른쪽 하단) 이용\n\n (기타문의 : 02-728-5636,5675,5679)\n\n");
		return;
	}
	
	function initPw(){
		window.open("/extra/InitPwUi.jsp", "POPUP", "title=no, status=0 , height=160, width=260, left=0, top=0, scrollbars=no");
	}
-->
</script>
</head>

<body background="/images/bg.gif"  topmargin="100" onLoad="MM_preloadImages('/images/btn-ok-click.gif','/images/btn-ok.gif');form.id.focus();setModify()">
<form name=form method="post">
<input type="hidden" name="command" value="login">
<input type="hidden" name="pageInfo" value="login.jsp">
<input type="hidden" name="loginCnt" value="0">
<table width="756" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td width="379" height="399" rowspan="4"><img src="/images/login-img1.gif" width="379" height="399"></td>
        <td width="369"><img src="/images/login-img2.gif" width="377" height="78"></td>
    </tr>
    <tr>
        <td width="377" height="81" background="/images/login-img3.gif">
			<table width="369" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="255" height="81" align="right" background="/images/login-img6.gif">
						<table width="145" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" valign="top"><input name="id" type="text" class="input" size="18" maxlength="20"></td>
                            </tr>
                            <tr>
                                <td height="30" valign="top" style="padding-top:3px"><input name="password" type="password" class="input" size="18"  maxlength="20" onkeypress="move(event.keyCode)"></td>
                            </tr>
                        </table>
					</td>
                    <td valign="top" style="padding-top:13px"><img src="/images/btn-ok.gif" name="Image1" width="65" height="48" id="Image1" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image1','','/images/btn-ok-click.gif',1)" onClick="login()"></td>
                </tr>
            </table>
		</td>
    </tr>
    <tr>
        <td><img src="/images/login-img5.gif" width="377" height="29" border="0" usemap="#Map1"></td>
    </tr>
    <tr>
        <td height="203"><img src="/images/login-img4.gif" width="377" height="211" ></td>
    </tr>
    <tr valign="bottom">
    	<td colspan="2" height="25" align="right" class="input"><font color="blue"><a href="javascript:initPw();" style="cursor:hand">[비밀번호 초기화]</a></font></td>
    </tr>
</table>
<map name="Map1">
    <area shape="rect" coords="233,3,352,22" href="/sales/customer/trcustinsert/TrCustInsertUI.jsp" target="_blank">
</map>
</body>
</html>
<%
    String msgtype		= request.getParameter("msgtype");
	String loginCnt		= request.getParameter("loginCnt");
	String remainDay	= ComUtil.checkString(request.getParameter("remainDay"));

	if (msgtype == null || msgtype.equals("")) {
	}else if (msgtype.equals("1")) {
%>
		<script language="JavaScript">
			alert("아이디와 비밀번호가 정확하지 않거나 입력하지 않은 것 같습니다.\n\n" +
		      	  "확인한 후에 다시 로그인해 주시기 바랍니다.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("2")) {
%>
		<script language="JavaScript">
			alert("아이디와 비밀번호가 맞지 않습니다.\n\n다시 한번 정확하게 입력해 주시기 바랍니다.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("3")) {
%>
		<script language="JavaScript">
			alert("데이터베이스와의 연결이 끊어진 것 같습니다.\n\n관리자에게 문의해보시기 바랍니다.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("4")) {
%>
		<script language="JavaScript">
			alert("비밀번호가 변경되었습니다.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("5")) {
%>
		<script language="JavaScript">
			alert("입력한 비밀번호가 일치하지 않습니다.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("6")) {
%>
		<script language="JavaScript">
			alert("이전 비밀번호가 일치하지 않습니다.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("7")) {
%>
		<script language="JavaScript">
			alert("해당 ID가 존재하지 않습니다.\n\n확인 후 다시 입력해 주십시오.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("8")) {
%>
		<script language="JavaScript">
			alert("인증되지 않은 아이디 입니다.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("9")) {
%>
		<script language="JavaScript">
			alert("LOG-IN 실패 횟수가 초과하였습니다.\n\n관리자(물류IT)에게 문의해보시기 바랍니다.");
			movePage();
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("10")) {
%>
		<script language="JavaScript">
			var msg = "비밀번호 변경 기간이 초과하였습니다.\n\n비밀번호를 변경하여 주십시오.";
			if(confirm(msg)){
				changepwd();
			}
			//alert("비밀번호 변경 기간이 초과하였습니다.\n\n비밀번호를 변경하여 주십시오.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("11")) {
%>
		<script language="JavaScript">
			var msg = "비밀번호 변경 이력이 미 존재 합니다.\n\n비밀번호를 변경하여 주십시오.";
			if(confirm(msg)){
				changepwd();
			}
			//alert("비밀번호 변경 이력이 미 존재 합니다.\n\n비밀번호를 변경하여 주십시오.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("12")) {
%>
		<script language="JavaScript">
			var msg = "ID와 비밀번호가 동일합니다.\n\n비밀번호를 변경하여 주십시오.";
			if(confirm(msg)){
				changepwd();
			}
			//alert("ID와 비밀번호가 동일합니다.\n\n비밀번호를 변경하여 주십시오.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("13")) {
%>
		<script language="JavaScript">
			var msg = "기존 변경한 비밀번호와 동일 합니다.\n\n비밀번호를 변경하여 주십시오.";
			if(confirm(msg)){
				changepwd();
			}
			//alert("기존 변경한 비밀번호와 동일 합니다.\n\n새로운 비밀번호를 입력하여 주십시오.");
		</script>
<%
	}else if(msgtype.equals("14")){
%>
		<script language="JavaScript">
			alert("비밀번호 변경 기간이 <%=remainDay%>일 남았습니다!");
		</script>
<%
	}
%>