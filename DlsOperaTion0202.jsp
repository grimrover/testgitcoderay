<%@ page language="java" contentType = "text/html;charset=KSC5601" %>
<%@ page import="dls.frame.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>디지털물류시스템 (Digital Logistics System)</title>
<style type="text/css">

</style>
<%
	String msgtype 		= request.getParameter("msgtype");
	String remainDay	= request.getParameter("remainDay");
	
	String Url = HttpUtils.getRequestURL(request).toString();
	
	if(!Url.equals("http://172.16.201.201/DlsOperaTion0202.jsp") &&
	   !Url.equals("http://172.16.201.201:7010/DlsOperaTion0202.jsp") &&
	   !Url.equals("http://172.16.201.202/DlsOperaTion0202.jsp") &&
	   !Url.equals("http://172.16.201.202:7010/DlsOperaTion0202.jsp") &&
	   !Url.equals("http://172.16.201.203/DlsOperaTion0202.jsp") &&
	   !Url.equals("http://172.16.201.203:7010/DlsOperaTion0202.jsp") &&
	   !Url.equals("http://localhost:7010/DlsOperaTion0202.jsp") &&
	   !Url.equals("http://127.0.0.1:7010/DlsOperaTion0202.jsp") &&
	   !Url.equals("http://211.210.94.11:7010/DlsOperaTion0202.jsp") &&
	   !Url.equals("http://211.210.94.12:7010/DlsOperaTion0202.jsp") &&
	   !Url.equals("http://localhost:7001/DlsOperaTion0202.jsp") &&
	   // [20140515] MKCHOI
	   !Url.equals("http://dls.hanjin.co.kr/DlsOperaTion0202.jsp")){
	   
%>
<script	language = "javascript">
	alert("현 페이지에 접속할 권한이 없습니다!");
	location.replace('/extra/extra_index.jsp');
</script>
<%
	}
	
	if (msgtype == null) {	
	}else if (msgtype.equals("1")) {
%>
		<script language="JavaScript">
			alert("아이디와 비밀번호가 정확하지 않거나 입력하지 않은 것 같습니다.\n\n" +
		      	  "확인한 후에 다시 로그인해 주시기 바랍니다!");
			location.replace('/DlsOperaTion0202.jsp');			
		</script>
<%
	}else if (msgtype.equals("2")) {
%>
		<script language="JavaScript">
			alert("아이디와 비밀번호가 맞지 않습니다.\n\n다시 한번 정확하게 입력해 주시기 바랍니다.");
			location.replace('/DlsOperaTion0202.jsp');			
		</script>
<%
	}else if (msgtype.equals("3")) {
%>
		<script language="JavaScript">
			alert("데이터베이스와의 연결이 끊어진 것 같습니다.\n\n관리자에게 문의해보시기 바랍니다.");
			location.replace('/DlsOperaTion0202.jsp');
			
		</script>
<%
	}else if (msgtype.equals("4")) {
%>
		<script language="JavaScript">
			alert("비밀번호가 변경되었습니다!");
			parent.window.location.reload(); 			
		</script>
<%
	}else if (msgtype.equals("5")) {
%>
		<script language="JavaScript">
			alert("입력한 비밀번호가 일치하지 않습니다.");
			location.replace('/DlsOperaTion0202.jsp');			
		</script>
<%
	}else if (msgtype.equals("6")) {
%>
		<script language="JavaScript">
			alert("이전 비밀번호가 일치하지 않습니다.");			
			location.replace('/DlsOperaTion0202.jsp');
		</script>
<%
	}else if (msgtype.equals("7")) {
%>
		<script language="JavaScript">
			alert("해당 ID가 존재하지 않습니다.\n\n확인 후 다시 입력해 주십시오.");
			location.replace('/DlsOperaTion0202.jsp');
			
		</script>
<%
	}else if (msgtype.equals("8")) {
%>
		<script language="JavaScript">
			alert("ID와 비밀번호가 동일합니다.\n\n비밀번호를 변경하여 주십시오.");	
			location.replace('/DlsOperaTion0202.jsp');	
		</script>
<%
	}else if (msgtype.equals("9")) {
%>
		<script language="JavaScript">
			alert("LOG-IN 실패 횟수가 초과하였습니다.\n\n관리자(물류IT)에게 문의해보시기 바랍니다.");
			location.replace('/DlsOperaTion0202.jsp');
		</script>
<%
	}else if (msgtype.equals("10")) {
%>
		<script language="JavaScript">
			alert("비밀번호 변경 기간이 초과하였습니다.\n\n비밀번호를 변경하여 주십시오.");
			location.replace('/DlsOperaTion0202.jsp');			
		</script>
<%
	}else if (msgtype.equals("11")) {
%>
		<script language="JavaScript">
			alert("비밀번호 변경 이력이 미 존재 합니다.\n\n비밀번호를 변경하여 주십시오.");
			location.replace('/DlsOperaTion0202.jsp');
		</script>
<%
	}else if (msgtype.equals("12")) {
%>
		<script language="JavaScript">
			alert("기존 변경한 비밀번호와 동일 합니다.\n\n새로운 비밀번호를 입력하여 주십시오.");
			location.replace('/DlsOperaTion0202.jsp');
		</script>
<%
	}
%>

<script language="JavaScript">
<!--
// -->
</script>
</head>
    <% if(msgtype == null){ %>
<frameset frameborder="NO" border="0" framespacing="0">    
    	<frame name="mainFrame" src="DlsMain.jsp">    	
    <%}else{%>    
<frameset rows="92,*" frameborder="NO" border="0" framespacing="0">    
		<frame name="topFrame" scrolling="NO" noresize src="top_new.jsp" >
    	<frame name="mainFrame" src="dlsmain_new.jsp?msgtype=<%=msgtype%>&remainDay=<%=remainDay%>">    	
    <%}%>
</frameset>
<noframes>
<body bgcolor="#FFFFFF" text="#000000">
</body>
</noframes>
</html>
