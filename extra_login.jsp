<%@ page language="java" contentType = "text/html;charset=KSC5601" %>
<%@ page import="dls.management.*" %>
<%@ page import="dls.frame.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>DLS  Digital Logistics System(HANJIN Extranet Login)</title>
<style type="text/css">
<!--
/* 공통 제어 시작 */
   html,body {width:100%;height:100%;}
	html,body,div,ul,li,dl,dt,dd,ol,p,h1,h2,h3,h4,h5,h6,form {margin:0;padding:0;font:normal 11px dotum,'돋움',verdana;color:#cedff5;line-height:1.5em;}
	ul,ol,dl {list-style:none}
	img {border:0;vertical-align:top;}
	a {font:normal 11px dotum,'돋움';color:#cedff5 ;text-decoration:none;}
	a:hover {font:normal 11px dotum,'돋움';color:#cedff5;text-decoration:underline;}
/* 공통 제어 마침 */

#wrap {
	position: relative;
	top:50%;
	left:50%;
	width:720px;
	height:406px;
	margin-left:-360px; /*** width 값의 1/2 ***/
	margin-top:-203px; /*** height 값의 1/2 ***/
	
}

.visual{
	border: solid 1px #dfdfdf;
	}
	
.contents{
	background:url(extra/images/contents_bg.jpg) no-repeat;
	width:720px;
	height:131px;
	float:left;
	text-align:center;
	}
	
.news{
	width:337px;
	float:left;
	padding:15px 0 0 43px;
	text-align:left;
	}
.news table.new_t{
	width:330px;
	letter-spacing:-1px;
	margin-top:5px;
	margin-left:-10px;
}
.news table.new_t tr td {
	height:17px;
}

.line{
	width:12px;
	float:left;
	}
	
.login{
	width:280px; 
	float:left;
	padding:20px 0 0 28px;
	margin-right:20px;
	*margin-left:-40px ;
	}
.login input.input01{
	float:left;
	vertical-align:top;
	color:#2c4eae;
	font-size:12px;
	background:#deeaf8;
	border-top:#3170bd solid 1px;
	border-left:#3170bd solid 1px;
	border-bottom:#deeaf8 solid 1px;
	border-right:#deeaf8 solid 1px;
	width:139px;
	height:18px;
	margin:5px 8px 0 0;
	padding:6px 0 0 3px;
}
.login input.btn{
	margin: 5px 0;
	*margin-top:-68px ;
      *margin-left:245px ;

	}
.login a {
	text-decoration: underline;
	float:right;
	margin-right:6px;
	letter-spacing:-1px;
	*margin-top:-10px ;
      *margin-right:-28px ;
	width:75px;
	}
.login p {
	width:210px; 
	}

label img {
	padding-top:10px;
	float:left;
	}
//-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) { //v4.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && document.getElementById) x=document.getElementById(n); return x;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	function move(a)
	{
	   if (a == 13){
			login();
	   }
	}

	function login() {
		document.form.target="_self";
		document.form.action="/servlets/management/LogInOutServlet3";
		document.form.submit();
	}

	function changepwd() {
		window.location.replace("/extra/extra_index.jsp?flag=pwChange");
	}
	
	function movePage(){
		window.location.replace("/extra/extra_index.jsp");
	}
	
	function setModify()
	{
		alert("○ 패스워드 정책 Rule 적용사항\n    ① 패스워드 정책 적용일: 2012년 6월 1일부\n\n    ② 패스워드 최소자리 : 10 자리 이상\n\n    ③ 패스워드 변경주기 : 30일 이하\n\n    ④ 패스워드 변경이력 : 직전 3개 패스워드 재사용금지\n\n    ⑤ 패스워드 입력실패 : 5회 이상 접근시도 실패 시 비활성화\n\n    ⑥ 패스워드 Rule : 문자 · 숫자조합, ID와 동일 패스워드 금지\n\n    ⑦ 개인정보항목 패스워드 사용 금지(주민등록번호 등)\n\n ○ 패스워드 비활성화 시[비밀번호 초기화](오른쪽 하단) 이용\n\n (기타문의 : 02-728-5675)\n\n");
		return;
	}
	
	function initPw(){
		window.open("/extra/InitPwUi.jsp", "POPUP", "title=no, status=0 , height=160, width=260, left=0, top=0, scrollbars=no");
	}
	
	function openJaga(){
		window.open("/sales/customer/trcustinsert/TrCustInsertUI.jsp", "POPUP", "title=no, status=0 , height=680, width=965, left=0, top=0, scrollbars=no");
	}
-->
</script>
</head>
<body>
	<div id="wrap">
	<!-- Visual영역 -->
		<div class="visual"><img src="extra/images/Visual_dls.jpg" alt="한진" /></div>
		<!-- //Visual영역 -->
			<div class="contents">
			<!-- 공지영역 -->
				<div class="news"><img src="extra/images/news.gif" alt="News" />
					<table class="new_t">
						<tr>
							<td><img src="extra/images/icon.gif" /></td>
							<td><a href="javascript:setModify();"><b>접근통제 내용확인</b></a></td>
						</tr>						
						<tr>
							<td><img src="extra/images/icon.gif" /></td>
							<td><a href="javascript:openJaga();"><b>자가운송업체등록</b></td>
						</tr>
					</table>				
				</div>			
			<!--// 공지영역 -->
			<div class="line"><img src="extra/images/line01.gif" width="12" height="111" /></div>
			<!--// ID/PW영역 -->
			<div class="login">
				<form name=form method="post">
					<input type="hidden" name="command" value="login">
					<input type="hidden" name="pageInfo" value="login.jsp">
					<input type="hidden" name="loginCnt" value="0">
				<p>
					<label><img src="extra/images/id.gif" alt="ID" title="" /></label>
					<input type="text" name="id" class="input01" />
				</p>
				<p>
					<label><img src="extra/images/pw.gif" alt="PW" title="" /></label>
					<input type="password" name="password" value="" maxlength="12"  onFocus="this.value='';" class="input01" />
				</p>
				<input type="image" src="extra/images/btn_login.gif" class="btn" onClick="login()" />
				<a href="javascript:initPw();">비밀번호초기화</a>				
				</form>	
			</div>
		<!--// ID/PW영역 -->
		</div>
		
	</div>

</body>
</html>
<%
    String msgtype		= request.getParameter("msgtype");
	String loginCnt		= request.getParameter("loginCnt");
	String remainDay	= ComUtil.checkString(request.getParameter("remainDay"));

	if (msgtype == null || msgtype.equals("")) {
	}else if (msgtype.equals("1")) {
%>
		<script language="JavaScript">
			alert("아이디와 비밀번호가 정확하지 않거나 입력하지 않은 것 같습니다.\n\n" +
		      	  "확인한 후에 다시 로그인해 주시기 바랍니다.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("2")) {
%>
		<script language="JavaScript">
			alert("아이디와 비밀번호가 맞지 않습니다.\n\n다시 한번 정확하게 입력해 주시기 바랍니다.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("3")) {
%>
		<script language="JavaScript">
			alert("데이터베이스와의 연결이 끊어진 것 같습니다.\n\n관리자에게 문의해보시기 바랍니다.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("4")) {
%>
		<script language="JavaScript">
			alert("비밀번호가 변경되었습니다.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("5")) {
%>
		<script language="JavaScript">
			alert("입력한 비밀번호가 일치하지 않습니다.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("6")) {
%>
		<script language="JavaScript">
			alert("이전 비밀번호가 일치하지 않습니다.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("7")) {
%>
		<script language="JavaScript">
			alert("해당 ID가 존재하지 않습니다.\n\n확인 후 다시 입력해 주십시오.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("8")) {
%>
		<script language="JavaScript">
			alert("인증되지 않은 아이디 입니다.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("9")) {
%>
		<script language="JavaScript">
			alert("LOG-IN 실패 횟수가 초과하였습니다.\n\n관리자(물류IT)에게 문의해보시기 바랍니다.");
			//movePage();
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("10")) {
%>
		<script language="JavaScript">
			var msg = "비밀번호 변경 기간이 초과하였습니다.\n\n비밀번호를 변경하여 주십시오.";
			if(confirm(msg)){
				changepwd();
			}
			//alert("비밀번호 변경 기간이 초과하였습니다.\n\n비밀번호를 변경하여 주십시오.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("11")) {
%>
		<script language="JavaScript">
			var msg = "비밀번호 변경 이력이 미 존재 합니다.\n\n비밀번호를 변경하여 주십시오.";
			if(confirm(msg)){
				changepwd();
			}
			//alert("비밀번호 변경 이력이 미 존재 합니다.\n\n비밀번호를 변경하여 주십시오.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("12")) {
%>
		<script language="JavaScript">
			var msg = "ID와 비밀번호가 동일합니다.\n\n비밀번호를 변경하여 주십시오.";
			if(confirm(msg)){
				changepwd();
			}
			//alert("ID와 비밀번호가 동일합니다.\n\n비밀번호를 변경하여 주십시오.");
			form.loginCnt.value = <%=loginCnt%>;
		</script>
<%
	}else if (msgtype.equals("13")) {
%>
		<script language="JavaScript">
			var msg = "기존 변경한 비밀번호와 동일 합니다.\n\n비밀번호를 변경하여 주십시오.";
			if(confirm(msg)){
				changepwd();
			}
			//alert("기존 변경한 비밀번호와 동일 합니다.\n\n새로운 비밀번호를 입력하여 주십시오.");
		</script>
<%
	}else if(msgtype.equals("14")){
%>
		<script language="JavaScript">
			alert("비밀번호 변경 기간이 <%=remainDay%>일 남았습니다!");
		</script>
<%
	}
%>